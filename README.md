# Office picker

> Jamie's vacation is about to end. She works at an international company, so she can choose another office to work from: either Amsterdam, Madrid, or Budapest. Help her choose which office to go to – she’d like someplace with good weather or cheap flights (or both).

## Details

I started by diving the general specifications into single points:

- Jamie works at an international company;
- her vacation is about to end, is not ended yet!
- available offices are Amsterdam, Madrid or Budapest;
- Jamie likes good weather and keeps an eye on budget. She is ending her vacation, so not in the mood of spending more.

## Assumptions

Based on the details I have so far, I can make the following assumptions:

- **mobile device**: she likes to travel and she's currently doing so. She is not using windows explorer for sure, but safari or chrome instead. Mobile first is a must, as like every respectful web app;
- **loading and connection error handling**: don't know where she is but connection could be slow, better to take loading into account (especially because of the APIs). A PWA could help, but she'll need to be online in order to see valuable results (if not, let's inform here to find some better connection but let's not be rude!);
- **location**: Jamie will need to go back to where she was working, take her things and do a moving. Current position is not so important, but where she was working matters. Because of friend and family, she could also think about doing some round trips between the old and new office. I can assume her old office is in the list, because then she can also understand if is better to stay where she is. Based on the text this assumption could be wrong, so maybe just better to give her the flexible option to select the departure location and also give valuable information about it (like weather and other data);
- Jamie will need to do her choice and contact the HR. This could be done via email, but would be cool to integrate a simple submit form/typeform so she can make her choice, communicate it and go back to relax. Only problem with this is security, because without any authentication system everyone could just pretend to be Jaime. So I could do proper auth or just add a mailto link which opens the email app. Let's go for the latter, so I can focus more on the goal and provide a secure app anyway thanks to the email security itself.

## UI/UX Flow sketch

Next, I did a basic wireframe to get a better vision on what I wanted to create in Figma.

Url: https://www.figma.com/file/2smzdwaITbutPt0zpd7d8N/Office-picker?node-id=0%3A1

<iframe style="border: none;" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2F2smzdwaITbutPt0zpd7d8N%2FOffice-picker%3Fnode-id%3D0%253A1" allowfullscreen></iframe>

This would be my ideal flow but it goes beyond the scope of the app and could overcomplicate things, so I decided to focus on the core comparison for the implementation.

## Tech

The tech used for the app are Angular and Firebase (just the cloud functions), plus Tailwind CSS for the styling with an utility-class first approach which I consider valuable and scalable for every design needs.

### Choices

- **Data visualization**: I choosed to provide limited data to don't confuse the user, is better to have average values than every single details. If he/she wants, there is an handy link to check specific details for the weather for example. The important is to provide an overall to make a choice. The horizontal bar chart quickly shows if a destination is cheap and with good weather, which is the main goal of this app
- **Backend REST API**: in order to keep the accuweather key not public and access third party API protected by CORS, a backend API is needed. This also give some advantages, like filtering from external API data that the frontend does not need (both APIs provide an huge amount of data, especially the kiwi one).
- **Testing**: code is changed frequently and the focus is on providing a functional solution, so I choosed not to do unit tests. If I would do them, API would be the first thing to test, then services/pipes and core components (like the horizontal bar chart). Jest would be my library of choice because of several benefits.

## Improvements

- remove magic number/values in the code, like the city locations key. Is better to group them in config files/enums.
- storybook
- create ad hoc components to improve reusage (e.g. a typeahead input)
- add shared code between frontend and backend (enums, interfaces, etc...)
- add index.html metadata (e.g. image, favicon, )
- extend the wrapper rest API with options by query params (e.g. interval for flights), plus adding cities that are now enconded in the frontend
- review images size and quality
- unit test REST API, unit test services and pipes and components (when makes sense, like a quite stable and reused component)
- better error handling (e.g. if internet/API is not available)
- add PWA capabilities (service worker, manifest, etc..)
- CI/CD pipeline
- add state manager like NGRX if app state start to get too messy and multiple components use same data source

## Where to find relevant code

- **UI**: you can refer mainly to the `city.component.ts` and some page style in the `comparison.component.ts`. Thanks to the utility-class approach, the UI is easily customizable without any "magic" classes which do too much or too less
- **Patterns**:
  - the container component pattern can be seen in the `city-container.component.ts`, the goal is to remove responsibility from the component itself (which ideally should be a dumb component) to fetch data.
  - the reactive pattern, mainly relying on `rxjs`, can be seen in the `comparison.component.ts` to react to the departure input changes (making sure http requests are not triggered for every keystroke).
  - the general programming style is functional programming, which can be seen mainly in the services (e.g. `city.service.ts`) by using piping `rxjs` operators
- **Animations**: the `horizontal-bar.component.ts` contains a small css animation to show the bar, while `comparison.component.ts` use an Angular animation when the city cards are in the DOM.

## Notes

- the accuweather API has a rate limit of 50 req/day. Every page load calls this API 6 times, so the limit can be quickly reached. If this happens, the endpoint currently provide some mock data. This is not true for the kiwi API, which does not have usage limits and should work every time.
