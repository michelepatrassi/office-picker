import axios from 'axios';
import { headers } from '../constants';

const BASE_URL = 'https://api.skypicker.com';

export const getLocations = async (term: string): Promise<{ data: any }> => {
  return axios.get(`${BASE_URL}/locations`, {
    headers,
    params: {
      term,
      locale: 'en-US',
      location_types: 'airport',
      limit: 200,
      active_only: true,
      sort: 'rank',
    },
  });
};

export const getFlights = async (from: string, to: string, dateFrom: Date, dateTo: Date): Promise<{ data: any }> => {
  return axios.get(`${BASE_URL}/flights`, {
    headers,
    params: {
      fly_from: from,
      fly_to: to,
      date_from: formatDate(dateFrom),
      date_to: formatDate(dateTo),
      partner: 'picky',
    },
  });
};

const formatDate = (date: Date): string => {
  return `${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`;
};
