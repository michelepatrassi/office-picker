import * as functions from 'firebase-functions';
import * as express from 'express';
import { FirebaseConfig } from '../models/firebase-config';

export const getFirebaseConfig = (): FirebaseConfig => {
  return functions.config() as FirebaseConfig;
};

export const getRandom = (items: any[]): any => {
  return items[Math.floor(Math.random() * items.length)];
};

export const handleHttpError = (e: any, res: express.Response): express.Response => {
  console.log(`handleHttpError - e`, e);

  if (e.response) {
    const { status, data } = e.response;
    return res.status(status).send(data);
  }

  return res.status(500).send({ title: 'Ouch, something bad happened!' });
};
