import { CURRENT_CONDITIONS, FORECAST } from './../mocks/accuweather';
import { getFirebaseConfig } from './mixed';
import axios from 'axios';
import { headers } from '../constants';

const BASE_URL = `http://dataservice.accuweather.com`;

export const getCurrentCondition = async (city: string): Promise<{ data: any }> => {
  const { key } = getFirebaseConfig().accuweather;
  const locationKey = mapCityToLocationKey(city);

  return axios
    .get<any>(`${BASE_URL}/currentconditions/v1/${locationKey}`, {
      headers,
      params: { apikey: key },
    })
    .catch((_) => ({ data: [CURRENT_CONDITIONS[locationKey]] }));
};

export const getForecast = async (city: string): Promise<{ data: any }> => {
  const { key } = getFirebaseConfig().accuweather;
  const locationKey = mapCityToLocationKey(city);

  return axios
    .get<any>(`${BASE_URL}/forecasts/v1/daily/5day/${locationKey}`, {
      headers,
      params: { apikey: key },
    })
    .catch((_) => {
      return { data: FORECAST };
    });
};

const mapCityToLocationKey = (city: string): number => {
  switch (city.toLowerCase()) {
    case 'amsterdam':
      return 249758;
    case 'madrid':
      return 308526;
    case 'budapest':
      return 187423;
    default:
      throw new Error(`Cannot find a corresponding location key for city ${city}`);
  }
};
