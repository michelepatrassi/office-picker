import * as functions from 'firebase-functions';

const DEFAULT_REGION = 'europe-west1';

export const accuweather = functions
  .region(DEFAULT_REGION)
  .https.onRequest(async (req, res) => await (await import('./http/accuweather')).default(req, res));

export const kiwi = functions
  .region(DEFAULT_REGION)
  .https.onRequest(async (req, res) => await (await import('./http/kiwi')).default(req, res));
