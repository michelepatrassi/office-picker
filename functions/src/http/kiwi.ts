import * as express from 'express';
import * as cors from 'cors';
import { handleHttpError } from '../helpers/mixed';
import { getLocations, getFlights } from '../helpers/kiwi';

const app = express();
app.use(cors({ origin: true }));

app.get('/locations/:term', async (req: express.Request, res: express.Response) => {
  try {
    const { term } = req.params;

    const resp = await getLocations(term).then((res) =>
      res.data.locations.map((location: any) => {
        const { id, name, city } = location;
        return {
          id,
          name,
          city: city.name,
        };
      }),
    );
    return res.status(200).send(resp);
  } catch (e) {
    return handleHttpError(e, res);
  }
});

app.get('/flights-price/', async (req: express.Request, res: express.Response) => {
  try {
    const { from, to } = req.query;

    const startDate = new Date();
    const endDate = new Date(startDate.getTime());
    endDate.setMonth(startDate.getMonth() + 3);

    const resp = await getFlights(from as string, to as string, startDate, endDate).then((res) =>
      res.data.data.map((data: any) => ({ value: data.conversion.EUR, unit: 'EUR' })),
    );
    return res.status(200).send(resp);
  } catch (e) {
    return handleHttpError(e, res);
  }
});

export default app;
