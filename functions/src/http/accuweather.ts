import { getForecast } from './../helpers/accuweather';
import * as express from 'express';
import * as cors from 'cors';
import { getCurrentCondition } from '../helpers/accuweather';
import { handleHttpError } from '../helpers/mixed';

const app = express();
app.use(cors({ origin: true }));

app.get('/current-condition/:city', async (req: express.Request, res: express.Response) => {
  try {
    const { city } = req.params;

    const resp = await getCurrentCondition(city);
    return res.status(200).send(resp.data);
  } catch (e) {
    return handleHttpError(e, res);
  }
});

app.get('/forecast/:city', async (req: express.Request, res: express.Response) => {
  try {
    const { city } = req.params;

    const resp = await getForecast(city);
    return res.status(200).send(resp.data);
  } catch (e) {
    return handleHttpError(e, res);
  }
});
export default app;
