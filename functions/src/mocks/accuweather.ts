export const FORECAST = {
  Headline: {
    MobileLink: 'http://m.accuweather.com/en/nl/amsterdam/249758/extended-weather-forecast/249758?lang=en-us',
    Link: 'http://www.accuweather.com/en/nl/amsterdam/249758/daily-weather-forecast/249758?lang=en-us',
  },
  DailyForecasts: [
    {
      Date: '2020-05-16T07:00:00+02:00',
      EpochDate: 1589605200,
      Temperature: {
        Minimum: {
          Value: 43.0,
          Unit: 'F',
          UnitType: 18,
        },
        Maximum: {
          Value: 59.0,
          Unit: 'F',
          UnitType: 18,
        },
      },
      Day: {
        Icon: 6,
        IconPhrase: 'Mostly cloudy',
        HasPrecipitation: false,
      },
      Night: {
        Icon: 35,
        IconPhrase: 'Partly cloudy',
        HasPrecipitation: false,
      },
    },
    {
      Date: '2020-05-17T07:00:00+02:00',
      EpochDate: 1589691600,
      Temperature: {
        Minimum: {
          Value: 50.0,
          Unit: 'F',
          UnitType: 18,
        },
        Maximum: {
          Value: 62.0,
          Unit: 'F',
          UnitType: 18,
        },
      },
      Day: {
        Icon: 3,
        IconPhrase: 'Partly sunny',
        HasPrecipitation: false,
      },
      Night: {
        Icon: 36,
        IconPhrase: 'Intermittent clouds',
        HasPrecipitation: false,
      },
    },
    {
      Date: '2020-05-18T07:00:00+02:00',
      EpochDate: 1589778000,
      Temperature: {
        Minimum: {
          Value: 51.0,
          Unit: 'F',
          UnitType: 18,
        },
        Maximum: {
          Value: 64.0,
          Unit: 'F',
          UnitType: 18,
        },
      },
      Day: {
        Icon: 3,
        IconPhrase: 'Partly sunny',
        HasPrecipitation: false,
      },
      Night: {
        Icon: 36,
        IconPhrase: 'Intermittent clouds',
        HasPrecipitation: false,
      },
    },
    {
      Date: '2020-05-19T07:00:00+02:00',
      EpochDate: 1589864400,
      Temperature: {
        Minimum: {
          Value: 48.0,
          Unit: 'F',
          UnitType: 18,
        },
        Maximum: {
          Value: 67.0,
          Unit: 'F',
          UnitType: 18,
        },
      },
      Day: {
        Icon: 2,
        IconPhrase: 'Mostly sunny',
        HasPrecipitation: false,
      },
      Night: {
        Icon: 34,
        IconPhrase: 'Mostly clear',
        HasPrecipitation: false,
      },
    },
    {
      Date: '2020-05-20T07:00:00+02:00',
      EpochDate: 1589950800,
      Temperature: {
        Minimum: {
          Value: 52.0,
          Unit: 'F',
          UnitType: 18,
        },
        Maximum: {
          Value: 71.0,
          Unit: 'F',
          UnitType: 18,
        },
      },
      Day: {
        Icon: 3,
        IconPhrase: 'Partly sunny',
        HasPrecipitation: false,
      },
      Night: {
        Icon: 33,
        IconPhrase: 'Clear',
        HasPrecipitation: false,
      },
    },
  ],
};

export const CURRENT_CONDITIONS: { [key: string]: any } = {
  249758: {
    WeatherText: 'Partly sunny',
    WeatherIcon: 3,
    HasPrecipitation: false,
    Temperature: {
      Metric: {
        Value: 12.8,
        Unit: 'C',
      },
    },
    MobileLink: 'http://m.accuweather.com/en/nl/amsterdam/249758/current-weather/249758?lang=en-us',
    Link: 'http://www.accuweather.com/en/nl/amsterdam/249758/current-weather/249758?lang=en-us',
  },
  308526: {
    WeatherText: 'Sunny',
    WeatherIcon: 1,
    HasPrecipitation: false,
    Temperature: {
      Metric: {
        Value: 19.9,
        Unit: 'C',
        UnitType: 17,
      },
    },
    MobileLink: 'http://m.accuweather.com/en/es/madrid/308526/current-weather/308526?lang=en-us',
    Link: 'http://www.accuweather.com/en/es/madrid/308526/current-weather/308526?lang=en-us',
  },
  187423: {
    WeatherText: 'Mostly cloudy',
    WeatherIcon: 6,
    HasPrecipitation: false,
    Temperature: {
      Metric: {
        Value: 23.2,
        Unit: 'C',
      },
    },
    MobileLink: 'http://m.accuweather.com/en/hu/budapest/187423/current-weather/187423?lang=en-us',
    Link: 'http://www.accuweather.com/en/hu/budapest/187423/current-weather/187423?lang=en-us',
  },
};
