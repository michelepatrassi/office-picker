import { Environment } from './environment.model';
import { NgxLoggerLevel } from 'ngx-logger';

export const environment: Environment = {
  production: true,
  mock: false,
  hmr: false,
  endpoint: `https://europe-west1-office-picker-6c563.cloudfunctions.net`,
  logLevel: NgxLoggerLevel.OFF,
};
