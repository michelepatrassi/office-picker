import { Environment } from './environment.model';
import { NgxLoggerLevel } from 'ngx-logger';

export const environment: Environment = {
  production: false,
  mock: true,
  hmr: true,
  endpoint: `http://localhost:5001/office-picker-6c563/europe-west1`,
  logLevel: NgxLoggerLevel.TRACE,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
