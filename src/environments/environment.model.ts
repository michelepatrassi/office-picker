import { NgxLoggerLevel } from 'ngx-logger';

export interface Environment {
  production: boolean;
  mock: boolean;
  hmr: boolean;
  endpoint: string;
  logLevel: NgxLoggerLevel;
}
