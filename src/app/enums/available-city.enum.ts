export enum AvailableCity {
  Amsterdam = 'amsterdam',
  Madrid = 'madrid',
  Budapest = 'budapest',
}
