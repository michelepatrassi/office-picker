export enum TemperatureUnit {
  celsius = 'Celsius',
  fahrenheit = 'Fahrenheit',
  celsiusSymbol = 'C',
  fahrenheitSymbol = 'F',
}
