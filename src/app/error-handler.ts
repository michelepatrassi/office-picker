import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  handleError(error: HttpErrorResponse | Error) {
    console.error(`error-handler.ts`, error);

    this.sendError(error);
  }

  private sendError(_) {
    //send error to sentry/something else
  }
}
