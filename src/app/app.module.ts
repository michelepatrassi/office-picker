import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComparisonComponent } from './pages/comparison/comparison.component';
import { HorizontalBarComponent } from './components/horizontal-bar.component';
import { CityComponent } from './components/city.component';
import { hmrOnInit, hmrOnDestroy, hmrAfterOnDestroy } from '../hmr';
import { TemperatureLabelPipe } from './pipes/temperature-label.pipe';
import { SpinnerComponent } from './components/spinner.component';
import { GlobalErrorHandler } from './error-handler';
import { CityContainerComponent } from './containers/city-container.component';
import { LoggerModule } from 'ngx-logger';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    ComparisonComponent,
    HorizontalBarComponent,
    CityComponent,
    TemperatureLabelPipe,
    SpinnerComponent,
    CityContainerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    LoggerModule.forRoot({ level: environment.logLevel }),
    NguiAutoCompleteModule,
    BrowserAnimationsModule,
  ],
  providers: [{ provide: ErrorHandler, useClass: GlobalErrorHandler }],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}

  hmrOnInit = (store) => hmrOnInit(store, this.appRef);
  hmrOnDestroy = (store) => hmrOnDestroy(store, this.appRef);
  hmrAfterDestroy = (store) => hmrAfterOnDestroy(store);
}
