import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ROUTES } from './constants/routes';
import { ComparisonComponent } from './pages/comparison/comparison.component';

const routes: Routes = [
  {
    path: ROUTES.home,
    component: ComparisonComponent,
  },
  { path: '**', redirectTo: `/${ROUTES.home}` },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
