import { Component } from '@angular/core';
import { CityService } from '../../services/city.service';
import { City } from '../../models/city.model';
import { Observable, of, Subject } from 'rxjs';
import { KiwiService } from '../../services/api/kiwi.service';
import { KiwiLocation } from '../../models/kiwi.model';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { fadeAnimation } from '../../animations/fade';

@Component({
  selector: 'app-comparison',
  templateUrl: './comparison.component.html',
  animations: [fadeAnimation],
})
export class ComparisonComponent {
  cities$: Observable<City[]>;
  readonly avgTemperatureMax = 35;
  readonly avgFlightsPriceMax = 300;
  departureAirport$ = new Subject<string>().pipe(debounceTime(300), distinctUntilChanged()) as Subject<string>;

  constructor(private cityService: CityService, private kiwiService: KiwiService) {
    this.cities$ = this.cityService.getAll();
  }

  getLifeCostMax(cities: City[]): number {
    const max = Math.max(...cities.map((city) => city.lifeCost.value));
    const offset = 30;
    return max + (max * offset) / 100;
  }

  trackByFn(index, city: City) {
    return city.name;
  }

  locations(keyword: string): Observable<KiwiLocation[]> {
    if (keyword) {
      return this.kiwiService.getLocations(keyword);
    } else {
      return of([]);
    }
  }

  locationFormatter(location: KiwiLocation): string {
    return `${location.name} (${location.id})`;
  }

  onDepartureLocationChanged(location: KiwiLocation) {
    this.departureAirport$.next(location.id);
  }
}
