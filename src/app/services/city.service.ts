import { AverageTemperature } from './../models/avg-temperature.model';
import { Injectable } from '@angular/core';
import { AvailableCity } from '../enums/available-city.enum';
import { Observable, of } from 'rxjs';
import { City } from '../models/city.model';
import { AccuWeatherService } from './api/accuweather.service';
import { CurrentCondition } from '../models/current-conditions.model';
import { map } from 'rxjs/operators';
import { ValueWithUnit } from '../models/value-with-unit.model';
import { KiwiService } from './api/kiwi.service';

const CITIES: City[] = [
  {
    name: AvailableCity.Amsterdam,
    imageSrc: 'assets/images/amsterdam.jpg',
    lifeCost: {
      value: 1500,
      unit: 'EUR/Month',
    },
    airportCode: 'AMS',
  },
  {
    name: AvailableCity.Madrid,
    imageSrc: 'assets/images/madrid.png',
    lifeCost: {
      value: 800,
      unit: 'EUR/Month',
    },
    airportCode: 'MAD',
  },
  {
    name: AvailableCity.Budapest,
    imageSrc: 'assets/images/budapest.jpg',
    lifeCost: {
      value: 1385,
      unit: 'EUR/Month',
    },
    airportCode: 'BUD',
  },
];

@Injectable({
  providedIn: 'root',
})
export class CityService {
  constructor(private accuweatherService: AccuWeatherService, private kiwiService: KiwiService) {}

  getAll(): Observable<City[]> {
    return of(CITIES);
  }

  getCurrentCondition(city: AvailableCity): Observable<CurrentCondition> {
    return this.accuweatherService.getCurrentCondition(city).pipe(
      map((currentCondition) => {
        const { WeatherText, Temperature, MobileLink, WeatherIcon } = currentCondition;
        const { Value, Unit } = Temperature.Metric;

        return {
          weatherText: WeatherText,
          weatherIconSrc: this.accuweatherService.getWeatherIconUrl(WeatherIcon),
          temperature: { value: Value, unit: Unit },
          url: MobileLink,
        };
      }),
    );
  }

  getAvgTemperature(city: AvailableCity): Observable<AverageTemperature> {
    return this.accuweatherService.getForecast(city).pipe(
      map((forecast) => {
        const { DailyForecasts, Headline } = forecast;

        const dailyAvgs = DailyForecasts.map((dailyForecast) => {
          const { Minimum, Maximum } = dailyForecast.Temperature;
          return (Minimum.Value + Maximum.Value) / 2;
        });
        const sum = dailyAvgs.reduce((acc, cur) => acc + cur, 0);
        const dailyAvg = sum / dailyAvgs.length;

        return {
          value: Math.round(dailyAvg * 10) / 10,
          unit: forecast.DailyForecasts[0].Temperature.Minimum.Unit,
          url: Headline.MobileLink,
        };
      }),
    );
  }

  getAvgFlightsPrice(fromAirportCode: string, toAirportCode: string): Observable<ValueWithUnit | null> {
    return this.kiwiService.getFlightPrices(fromAirportCode, toAirportCode).pipe(
      map((data) => {
        if (data && data.length) {
          const sum = data.reduce((acc, cur) => acc + cur.value, 0);
          const avg = sum / data.length;

          return {
            value: Math.round(avg * 10) / 10,
            unit: data[0].unit,
          };
        }

        return null;
      }),
    );
  }
}
