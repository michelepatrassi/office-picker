import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { NGXLogger } from 'ngx-logger';
import { tap } from 'rxjs/operators';
import { KiwiLocation } from '../../models/kiwi.model';
import { ValueWithUnit } from '../../models/value-with-unit.model';

@Injectable({
  providedIn: 'root',
})
export class KiwiService {
  readonly namespace = 'kiwi';

  constructor(private http: HttpClient, private logger: NGXLogger) {}

  getLocations(term: string): Observable<KiwiLocation[]> {
    this.logger.log('kiwi.service.ts - getLocations: term', term);

    return this.http
      .get<KiwiLocation[]>(`${environment.endpoint}/${this.namespace}/locations/${term}`)
      .pipe(tap((resp) => this.logger.log('kiwi.service.ts - getLocations: resp', resp)));
  }

  getFlightPrices(from: string, to: string): Observable<ValueWithUnit[]> {
    this.logger.log(`kiwi.service.ts - getFlightPrices: from ${from} to ${to}`);

    return this.http
      .get<ValueWithUnit[]>(`${environment.endpoint}/${this.namespace}/flights-price`, {
        params: { from, to },
      })
      .pipe(tap((resp) => this.logger.log('kiwi.service.ts - getFlightPrices: resp', resp)));
  }
}
