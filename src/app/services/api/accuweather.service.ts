import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AvailableCity } from '../../enums/available-city.enum';
import { environment } from '../../../environments/environment';
import { AccuweatherForecast, AccuweatherCurrentCondition } from '../../models/accuweather.model';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { TemperatureUnit } from '../../enums/temperature-unit.enum';
import { NGXLogger } from 'ngx-logger';

@Injectable({
  providedIn: 'root',
})
export class AccuWeatherService {
  readonly namespace = 'accuweather';

  constructor(private http: HttpClient, private logger: NGXLogger) {}

  getForecast(cityName: AvailableCity): Observable<AccuweatherForecast> {
    this.logger.log('accuweather.service.ts - getForecast: cityName', cityName);

    return this.http.get<AccuweatherForecast>(`${environment.endpoint}/${this.namespace}/forecast/${cityName}`).pipe(
      tap((resp) => this.logger.log('accuweather.service.ts - getForecast: resp', resp)),
      map((forecast) => {
        return {
          ...forecast,
          DailyForecasts: [
            ...forecast.DailyForecasts.map((dailyForecast) => ({
              ...dailyForecast,
              Temperature: {
                ...dailyForecast.Temperature,
                Minimum: this.castFahrenheitToCelsius(dailyForecast.Temperature.Minimum),
                Maximum: this.castFahrenheitToCelsius(dailyForecast.Temperature.Maximum),
              },
            })),
          ],
        };
      }),
    );
  }

  getCurrentCondition(cityName: AvailableCity): Observable<AccuweatherCurrentCondition> {
    this.logger.log('accuweather.service.ts - getCurrentCondition: cityName', cityName);

    return this.http
      .get<AccuweatherCurrentCondition[]>(`${environment.endpoint}/${this.namespace}/current-condition/${cityName}`)
      .pipe(
        tap((resp) => this.logger.log('accuweather.service.ts - getCurrentCondition: resp', resp)),
        map((currentCondition) => {
          if (currentCondition && currentCondition.length) {
            return currentCondition[0];
          }

          return null;
        }),
      );
  }

  getWeatherIconUrl(weatherIcon: number): string {
    const sanitizedVal = weatherIcon > 9 ? `${weatherIcon}` : `${weatherIcon}`;
    return `https://developer.accuweather.com/sites/default/files/0${sanitizedVal}-s.png`;
  }

  private castFahrenheitToCelsius(data: {
    Value: number;
    Unit: TemperatureUnit;
  }): { Value: number; Unit: TemperatureUnit.celsiusSymbol } {
    const { Value, Unit } = data;
    if (Unit === TemperatureUnit.fahrenheitSymbol) {
      return {
        ...data,
        Value: ((Value - 32) * 5) / 9,
        Unit: TemperatureUnit.celsiusSymbol,
      };
    }

    return data as { Value: number; Unit: TemperatureUnit.celsiusSymbol };
  }
}
