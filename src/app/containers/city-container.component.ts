import { AverageTemperature } from './../models/avg-temperature.model';
import { CurrentCondition } from './../models/current-conditions.model';
import { City } from './../models/city.model';
import { Component, OnInit, Input } from '@angular/core';
import { CityService } from '../services/city.service';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-city-container',
  template: `
    <app-city
      [city]="city"
      [lifeCostMax]="lifeCostMax"
      [avgTemperatureMax]="avgTemperatureMax"
      [avgFlightsPriceMax]="avgFlightsPriceMax"
      [currentCondition]="currentCondition$ | async"
      [avgTemperature]="avgTemperature$ | async"
      [avgFlightsPrice]="avgFlightsPrice$ | async"
      [isAvgFlightsPriceLoading]="isAvgFlightsPriceLoading"
    ></app-city>
  `,
})
export class CityContainerComponent implements OnInit {
  @Input() city: City;
  @Input() lifeCostMax: number;
  @Input() avgTemperatureMax: number;
  @Input() avgFlightsPriceMax: number;
  @Input() set departureAirportCode(code: string) {
    if (this.city && code) {
      this.isAvgFlightsPriceLoading = true;
      this.avgFlightsPrice$ = this.cityService.getAvgFlightsPrice(code, this.city.airportCode).pipe(
        catchError((e) => {
          this.isAvgFlightsPriceLoading = false;
          this.logger.error('city-container.component.ts - e', e);
          return of(null);
        }),
        tap(() => (this.isAvgFlightsPriceLoading = false)),
      );
    }
  }
  currentCondition$: Observable<CurrentCondition>;
  avgTemperature$: Observable<AverageTemperature>;
  avgFlightsPrice$: Observable<{ value: number; unit: string }>;
  isAvgFlightsPriceLoading = false;

  constructor(private cityService: CityService, private logger: NGXLogger) {}

  ngOnInit(): void {
    if (!this.city) {
      throw new Error(`City is required!`);
    }

    this.currentCondition$ = this.cityService.getCurrentCondition(this.city.name);
    this.avgTemperature$ = this.cityService.getAvgTemperature(this.city.name);
  }
}
