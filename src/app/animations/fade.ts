import { trigger, style, state, transition, animate } from '@angular/animations';

export const fadeAnimation = trigger('fadeAnimation', [
  state('visible', style({ opacity: 1 })),
  transition(':enter', [style({ opacity: 0 }), animate(1000)]),
  transition(':leave', animate(1000, style({ opacity: 0 }))),
]);
