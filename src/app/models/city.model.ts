import { AvailableCity } from '../enums/available-city.enum';

export interface City {
  name: AvailableCity;
  imageSrc: string;
  lifeCost: {
    value: number;
    unit: string;
  };
  airportCode: string;
}
