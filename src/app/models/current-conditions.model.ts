export interface CurrentCondition {
  weatherText: string;
  weatherIconSrc: string;
  temperature: {
    value: number;
    unit: string;
  };
  url: string;
}
