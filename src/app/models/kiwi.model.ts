export interface KiwiLocation {
  id: string;
  name: string;
  city: string;
}
