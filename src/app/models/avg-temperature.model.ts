import { ValueWithUnit } from './value-with-unit.model';

export interface AverageTemperature extends ValueWithUnit {
  url: string;
}
