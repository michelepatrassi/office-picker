import { TemperatureUnit } from '../enums/temperature-unit.enum';
export interface AccuweatherForecast {
  Headline: {
    EffectiveDate: string;
    MobileLink: string;
    Link: string;
  };
  DailyForecasts: AccuweatherDailyForecast[];
}

export interface AccuweatherDailyForecast {
  Date: string;
  EpochDate: number;
  Temperature: {
    Minimum: {
      Value: number;
      Unit: TemperatureUnit.fahrenheitSymbol | TemperatureUnit.celsiusSymbol;
    };
    Maximum: {
      Value: number;
      Unit: TemperatureUnit.fahrenheitSymbol | TemperatureUnit.celsiusSymbol;
    };
  };
  Day: {
    Icon: number;
    IconPhrase: string;
    HasPrecipitation: boolean;
  };
  Night: {
    Icon: number;
    IconPhrase: string;
    HasPrecipitation: boolean;
  };
}

export interface AccuweatherCurrentCondition {
  WeatherText: string;
  HasPrecipitation: boolean;
  Temperature: {
    Metric: {
      Value: number;
      Unit: string;
    };
  };
  WeatherIcon: number;
  MobileLink: string;
  Link: string;
}
