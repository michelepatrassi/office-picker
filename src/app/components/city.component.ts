import { AverageTemperature } from './../models/avg-temperature.model';
import { CurrentCondition } from './../models/current-conditions.model';
import { Component, Input, OnInit } from '@angular/core';
import { City } from '../models/city.model';
import { ValueWithUnit } from '../models/value-with-unit.model';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
})
export class CityComponent implements OnInit {
  @Input() city: City;
  @Input() lifeCostMax: number;
  @Input() avgTemperatureMax: number;
  @Input() avgFlightsPriceMax: number;
  @Input() currentCondition: CurrentCondition;
  @Input() avgTemperature: AverageTemperature;
  @Input() avgFlightsPrice: ValueWithUnit;
  @Input() isAvgFlightsPriceLoading = false;

  get lifeCostLabel(): string {
    return `${this.city.lifeCost.value} ${this.city.lifeCost.unit}`;
  }

  ngOnInit() {
    if (!this.city) {
      throw new Error(`City is required!`);
    }
  }
}
