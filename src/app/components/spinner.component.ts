import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-spinner',
  template: `<img [ngClass]="widthClass" class="spin h-auto" src="assets/icons/spinner.svg" /> `,
  styles: [
    `
      .spin {
        animation: rotation 1.5s infinite linear;
      }

      @keyframes rotation {
        from {
          transform: rotate(0deg);
        }
        to {
          transform: rotate(359deg);
        }
      }
    `,
  ],
})
export class SpinnerComponent {
  @Input() widthClass = 'w-16';
}
