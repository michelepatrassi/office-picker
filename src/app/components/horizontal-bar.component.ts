import { Component, Input } from '@angular/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-horizontal-bar',
  templateUrl: './horizontal-bar.component.html',
})
export class HorizontalBarComponent {
  @Input() value: number;
  @Input() label: string;
  @Input() max: number;
  @Input() isInverted = false;
  @Input() set percentage(percentage: number) {
    of(null)
      .pipe(delay(500))
      .subscribe(() => {
        if (this.isInverted) {
          return (this._percentage = 100 - percentage);
        }

        return (this._percentage = percentage);
      });
  }
  _percentage = 0;

  get barColorClass(): string {
    const base = 33.3;
    if (this._percentage < base) {
      return 'bg-orange-500';
    }

    if (this._percentage > base * 2) {
      return 'bg-green-500';
    }

    return 'bg-yellow-500';
  }

  get threshold(): number {
    return this.max / 2;
  }
}
