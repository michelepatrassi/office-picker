import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'temperatureLabel',
})
export class TemperatureLabelPipe implements PipeTransform {
  transform(input: { value: number; unit: string }): string {
    const { value, unit } = input;

    return `${value}${this.transformUnit(unit)}`;
  }

  private transformUnit(unit: string) {
    switch (unit.toLowerCase()) {
      case 'celsius':
      case 'c':
        return '°C';
      default:
        throw new Error(`Unsupported unit ${unit}`);
    }
  }
}
